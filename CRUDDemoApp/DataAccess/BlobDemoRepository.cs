﻿using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Blob;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace CRUDDemoApp.DataAccess
{
    public class BlobDemoRepository
    {
        const string ContainerName = "demoblob";
        private readonly CloudStorageAccount _cloudStorageAccount;
        private CloudBlobClient _blobClient;
        private CloudBlobClient Client => _blobClient ?? (_blobClient = _cloudStorageAccount.CreateCloudBlobClient());

        public BlobDemoRepository(CloudStorageAccount cloudStorageAccount)
        {
            _cloudStorageAccount = cloudStorageAccount;
        }

        public async Task<ICollection<(string name, string uri)>> ListFiles()
        {
            var result = new List<(string name, string uri)>();
            var container = await GetContainer();
            BlobContinuationToken token = null;
            do
            {
                BlobResultSegment resultSegment = await container.ListBlobsSegmentedAsync(token);
                token = resultSegment.ContinuationToken;

                foreach (IListBlobItem item in resultSegment.Results)
                {
                    if (item is CloudBlockBlob blob)
                    {
                        result.Add((blob.Name, blob.Uri.AbsolutePath));
                    }
                }
            } while (token != null);

            return result;
        }

        public async Task UploadBlob(string blobName, Stream content)
        {
            var blockBlob = (await GetContainer()).GetBlockBlobReference(blobName);
            await blockBlob.UploadFromStreamAsync(content);
        }

        public async Task<Stream> DownloadBlob(string blobName)
        {
            CloudBlockBlob blockBlob = (await GetContainer()).GetBlockBlobReference(blobName);
            var stream = new MemoryStream();
            await blockBlob.DownloadToStreamAsync(stream);
            stream.Position = 0;
            return stream;
        }

        public async Task DeleteBlob(string blobName)
        {
            var blockBlob = (await GetContainer()).GetBlockBlobReference(blobName);
            await blockBlob.DeleteAsync();
        }

        private async Task<CloudBlobContainer> GetContainer()
        {
            CloudBlobContainer container = Client.GetContainerReference(ContainerName);
            await container.CreateIfNotExistsAsync();
            return container;
        }
    }
}

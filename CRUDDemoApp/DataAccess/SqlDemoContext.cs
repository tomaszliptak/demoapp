﻿using CRUDDemoApp.Entities;
using Microsoft.EntityFrameworkCore;

namespace CRUDDemoApp.DataAccess
{
    public class SqlDemoContext : DbContext
    {
        public SqlDemoContext() { }

        public SqlDemoContext(DbContextOptions<SqlDemoContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Person> Persons { get; set; }

        public virtual void SetModified(object entity)
        {
            Entry(entity).State = EntityState.Modified;
        }

        public virtual void SetDeleted(object entity)
        {
            Entry(entity).State = EntityState.Deleted;
        }
    }
}

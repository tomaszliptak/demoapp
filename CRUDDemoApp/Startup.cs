﻿using System;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.EntityFrameworkCore;
using CRUDDemoApp.DataAccess;
using Microsoft.WindowsAzure.Storage;
using Microsoft.Azure.Documents.Client;

namespace CRUDDemoApp
{
    public class Startup
    {
        public Startup(IHostingEnvironment env)
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(env.ContentRootPath)
                .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true)
                .AddJsonFile($"appsettings.{env.EnvironmentName}.json", optional: true)
                .AddEnvironmentVariables();
            Configuration = builder.Build();
        }

        public IConfigurationRoot Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddDbContext<SqlDemoContext>(dbContextOptions =>
                dbContextOptions.UseSqlServer(Configuration.GetConnectionString("SqlConnection"), 
                options => options.EnableRetryOnFailure()));

            services.AddTransient(provider => CloudStorageAccount.Parse(Configuration.GetConnectionString("BlobConnection")));
            services.AddTransient<BlobDemoRepository>();

            services.AddTransient(provider =>
            {
                var endpointUri = Configuration.GetValue<string>("DocumentDb:EndpointUri");
                var primaryKey = Configuration.GetValue<string>("DocumentDb:PrimaryKey");
                var client = new DocumentClient(new Uri(endpointUri), primaryKey);
                return client;
            });

            // Add framework services.
            services.AddMvc();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory)
        {
            loggerFactory.AddConsole(Configuration.GetSection("Logging"));
            loggerFactory.AddDebug();

            app.UseMvc();
        }
    }
}

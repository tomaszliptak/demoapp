﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using CRUDDemoApp.DataAccess;

namespace CRUDDemoApp.Controllers
{
    [Route("api/[controller]")]
    public class BlobController : Controller
    {
        BlobDemoRepository _blobDemoRepository;

        public BlobController(BlobDemoRepository blobDemoRepository)
        {
            _blobDemoRepository = blobDemoRepository;
        }

        // GET api/values
        [HttpGet]
        public async Task<IEnumerable<(string name, string uri)>> Get()
        {
            return await _blobDemoRepository.ListFiles();
        }

        // GET api/values/5
        [HttpGet("{fileName}")]
        public async Task<FileStreamResult> Get(string fileName)
        {
            return File(await _blobDemoRepository.DownloadBlob(fileName), "application/octet-stream");
        }

        // PUT api/values/5
        [HttpPut("{fileName}")]
        public async Task Put(string fileName)
        {
            await _blobDemoRepository.UploadBlob(fileName, Request.Body);
        }

        // DELETE api/values/5
        [HttpDelete("{fileName}")]
        public async Task Delete(string fileName)
        {
            await _blobDemoRepository.DeleteBlob(fileName);
        }
    }
}

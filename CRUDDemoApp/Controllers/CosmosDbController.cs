﻿using CRUDDemoApp.Entities;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Azure.Documents;
using Microsoft.Azure.Documents.Client;
using System;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;

namespace CRUDDemoApp.Controllers
{
    [Route("api/[controller]")]
    public class CosmosDbController : Controller
    {
        private const string DbName = "cosmosdb";
        private const string CollectionId = "Persons";
        private readonly DocumentClient _documentClient;

        public CosmosDbController(DocumentClient documentClient)
        {
            _documentClient = documentClient;
        }

        // GET api/values
        [HttpGet]
        public async Task<IEnumerable<Person>> Get()
        {
            throw new NotImplementedException();
        }

        // GET api/values/5
        [HttpGet("{id}")]
        public async Task<IActionResult> Get(string id)
        {
            try
            {
                return Json((await _documentClient.ReadDocumentAsync<PersonCosmos>(GetDocUri(id))).Document);
            }
            catch (DocumentClientException de)
            {
                if (de.StatusCode == HttpStatusCode.NotFound)
                {
                    return NotFound();
                }
                throw;
            }
        }

        // PUT api/values/5
        [HttpPost]
        public async Task Post([FromBody]PersonCosmos newPerson)
        {
            await _documentClient.CreateDocumentAsync(GetCollUri(), newPerson);
        }

        // PUT api/values/5
        [HttpPut("{id}")]
        public async Task Put(string id, [FromBody]PersonCosmos person)
        {
            await _documentClient.ReplaceDocumentAsync(GetDocUri(person.Id), person);
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public async Task Delete(string id)
        {
            await _documentClient.DeleteDocumentAsync(GetDocUri(id));
        }

        private Uri GetDocUri(string documentId)
        {
            return UriFactory.CreateDocumentUri(DbName, CollectionId, documentId);
        }
        private Uri GetCollUri()
        {
            return UriFactory.CreateDocumentCollectionUri(DbName, CollectionId);
        }
    }
}

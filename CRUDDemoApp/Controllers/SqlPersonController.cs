﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using CRUDDemoApp.DataAccess;
using CRUDDemoApp.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Http;

namespace CRUDDemoApp.Controllers
{
    // ToDo: Dto should be returned but entity is returned instead (lack of time).
    [Route("api/[controller]")]
    public class SqlPersonController : Controller
    {
        SqlDemoContext _context;

        public SqlPersonController(SqlDemoContext context)
        {
            _context = context;
        }

        // GET api/values
        [HttpGet]
        public async Task<IEnumerable<Person>> Get()
        {
            return await _context.Persons.ToListAsync();
        }

        // GET api/values/5
        [HttpGet("{id}")]
        public async Task<Person> Get(int id)
        {
            return await _context.Persons.FirstOrDefaultAsync(p => p.Id == id);
        }

        // POST api/values - for Create
        [HttpPost]
        public async Task<StatusCodeResult> Post([FromBody]Person newPerson)
        {
            await _context.Persons.AddAsync(newPerson);
            await _context.SaveChangesAsync();
            return Ok();
        }

        // PUT api/values/5 - for Update
        [HttpPut("{id}")]
        public async Task<StatusCodeResult> Put(int id, [FromBody]Person recordToUpdate)
        {
            try
            {
                recordToUpdate.Id = id;
                _context.SetModified(recordToUpdate);
                await _context.SaveChangesAsync();
                return Ok();
            }
            catch (DbUpdateConcurrencyException)
            {
                // Action + Logging
                return StatusCode(StatusCodes.Status409Conflict);
            }
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public async Task<StatusCodeResult> Delete(int id)
        {
            try
            {
                var personToDelete = new Person { Id = id };
                _context.SetDeleted(personToDelete);
                await _context.SaveChangesAsync();
                return Ok();
            }
            catch (DbUpdateConcurrencyException)
            {
                // Action + Logging
                return StatusCode(StatusCodes.Status400BadRequest);
            }
        }
    }
}

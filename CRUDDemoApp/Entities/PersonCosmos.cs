﻿using Newtonsoft.Json;

namespace CRUDDemoApp.Entities
{
    public class PersonCosmos
    {
        [JsonProperty(PropertyName = "id")]
        public string Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
    }
}

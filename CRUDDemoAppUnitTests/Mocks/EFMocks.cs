﻿using CRUDDemoApp.DataAccess;
using CRUDDemoApp.Entities;
using CRUDDemoAppUnitTests.TestQueryProvider;
using Microsoft.EntityFrameworkCore;
using Moq;
using System.Collections.Generic;
using System.Linq;

namespace CRUDDemoAppUnitTests.Mocks
{
    class EFMocks
    {
        public static Mock<DbSet<TEntity>> CreateDbSetMock<TEntity>(IQueryable<TEntity> mockData) where TEntity : class
        {
            var mockSet = new Mock<DbSet<TEntity>>();

            mockSet.As<IAsyncEnumerable<TEntity>>()
                .Setup(m => m.GetEnumerator())
                .Returns(new TestAsyncEnumerator<TEntity>(mockData.GetEnumerator()));

            mockSet.As<IQueryable<TEntity>>()
               .Setup(m => m.Provider)
               .Returns(new TestAsyncQueryProvider<TEntity>(mockData.Provider));

            mockSet.As<IQueryable<TEntity>>().Setup(m => m.Expression).Returns(mockData.Expression);
            mockSet.As<IQueryable<TEntity>>().Setup(m => m.ElementType).Returns(mockData.ElementType);
            mockSet.As<IQueryable<TEntity>>().Setup(m => m.GetEnumerator()).Returns(() => mockData.GetEnumerator());

            return mockSet;
        }
    }
}

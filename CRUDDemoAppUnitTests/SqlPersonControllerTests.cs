using CRUDDemoApp.Controllers;
using CRUDDemoApp.DataAccess;
using CRUDDemoApp.Entities;
using CRUDDemoAppUnitTests.Mocks;
using CRUDDemoAppUnitTests.TestQueryProvider;
using FluentAssertions;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Xunit;

namespace CRUDDemoAppUnitTests
{
    public class SqlPersonControllerTests
    {
        [Fact]
        public async Task Verify_GetBy_ReturnsAllObjects()
        {
            // Arrange
            var mockData = new List<Person>{
                new Person{Id = 1 },
                new Person{Id = 2 },
                new Person{Id = 3 }
            }.AsQueryable();

            var mockContext = CreateSqlDemoContext(mockData);
            var service = new SqlPersonController(mockContext.Object);

            // Act
            var result = await service.Get();

            // Assert
            result.Should().NotBeNullOrEmpty()
                .And.HaveCount(3);
        }

        [Fact]
        public async Task Verify_GetById_ReturnsObjectWithProperId()
        {
            // Arrange
            var mockData = new List<Person>{
                new Person{Id = 1 },
                new Person{Id = 2 },
                new Person{Id = 3 }
            }.AsQueryable();

            var mockContext = CreateSqlDemoContext(mockData);
            var service = new SqlPersonController(mockContext.Object);

            // Act
            var person = await service.Get(1);

            // Assert
            person.Id.ShouldBeEquivalentTo(1);
        }

        [Fact]
        public async Task Verify_GetById_DoesNotReturnObjectForInvalidId()
        {
            // Arrange
            var mockData = new List<Person>{
                new Person{Id = 1 },
                new Person{Id = 2 },
                new Person{Id = 3 }
            }.AsQueryable();

            var mockContext = CreateSqlDemoContext(mockData);
            var service = new SqlPersonController(mockContext.Object);

            // Act
            var person = await service.Get(4);


            // Assert
            person.Should().BeNull();
        }

        [Fact]
        public async Task Verify_Post_CreatesNewObject()
        {
            // Arrange
            var newPerson = new Person
            {
                Id = 100,
                FirstName = "Tom",
                LastName = "Cruise"
            };

            var mockData = new List<Person>().AsQueryable();
            var mockSet = EFMocks.CreateDbSetMock(mockData);
            var mockContext = new Mock<SqlDemoContext>();
            mockContext.Setup(m => m.Persons).Returns(mockSet.Object);
            var service = new SqlPersonController(mockContext.Object);


            // Act
            var result = await service.Post(newPerson);

            // Assert
            mockSet.Verify(m => m.AddAsync(It.IsAny<Person>(), It.IsAny<System.Threading.CancellationToken>()), Times.Once());
            mockContext.Verify(m => m.SaveChangesAsync(It.IsAny<CancellationToken>()), Times.Once());
            result.StatusCode.ShouldBeEquivalentTo(StatusCodes.Status200OK);
        }

        [Fact]
        public async Task Verify_PutWithId_UpdatesEntityAndSavesData()
        {
            // Arrange
            var personToUpdate = new Person
            {
                FirstName = "Tom",
                LastName = "Cruise"
            };

            var mockData = new List<Person>().AsQueryable();
            var mockContext = new Mock<SqlDemoContext>();
            var service = new SqlPersonController(mockContext.Object);

            // Act
            var result = await service.Put(2, personToUpdate);

            // Assert
            mockContext.Verify(m => m.SetModified(It.IsAny<Person>()), Times.Once());
            mockContext.Verify(m => m.SaveChangesAsync(It.IsAny<CancellationToken>()), Times.Once());
            result.StatusCode.ShouldBeEquivalentTo(StatusCodes.Status200OK);
        }

        [Fact]
        public async Task Verify_DeleteWithId_SetEntityAsDeletedAndSavesData()
        {
            // Arrange
            var mockData = new List<Person>().AsQueryable();
            var mockContext = new Mock<SqlDemoContext>();
            var service = new SqlPersonController(mockContext.Object);

            // Act
            var result = await service.Delete(2);

            // Assert
            mockContext.Verify(m => m.SetDeleted(It.IsAny<Person>()), Times.Once());
            mockContext.Verify(m => m.SaveChangesAsync(It.IsAny<CancellationToken>()), Times.Once());
            result.StatusCode.ShouldBeEquivalentTo(StatusCodes.Status200OK);
        }

        private Mock<SqlDemoContext> CreateSqlDemoContext(IQueryable<Person> mockData)
        {
            var mockSet = EFMocks.CreateDbSetMock(mockData);
            var mockContext = new Mock<SqlDemoContext>();
            mockContext.Setup(m => m.Persons).Returns(mockSet.Object);
            return mockContext;
        }
    }
}
